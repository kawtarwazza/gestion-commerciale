﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Folder : GesComPME
    {
        [TestMethod]
        public void Test_Create_File()
        {
             /*var wait = new DefaultWait<WindowsDriver<WindowsElement>>(MySession)
             {
                 Timeout = TimeSpan.FromSeconds(60),
             };
             wait.IgnoreExceptionTypes(typeof(InvalidOperationException));

             WindowsElement mainWindow = null;

             wait.Until(MySession =>
             {
                 MySession.SwitchTo().Window(MySession.WindowHandles[0]);

                 mainWindow = MySession.FindElementByAccessibilityId("WelcomeDialog");

                 return mainWindow != null;
             });*/
            Thread.Sleep(10000);

            
                MySession.SwitchTo().Window(MySession.WindowHandles[0]);
                MySession.FindElementByName("Créer un nouveau dossier").Click();
                MySession.SwitchTo().Window(MySession.WindowHandles[0]);
                MySession.FindElementByAccessibilityId("nextButton").Click();

                MySession.SwitchTo().Window(MySession.WindowHandles[0]);
                MySession.FindElementByAccessibilityId("nextButton").Click();

                MySession.SwitchTo().Window(MySession.WindowHandles[0]);
                MySession.FindElementByAccessibilityId("databaseNameEditor").SendKeys("ll");
                MySession.FindElementByAccessibilityId("nextButton").Click();

                MySession.SwitchTo().Window(MySession.WindowHandles[0]);
                MySession.FindElementByAccessibilityId("nextButton").Click();

                MySession.SwitchTo().Window(MySession.WindowHandles[0]);
                MySession.FindElementByAccessibilityId("importZipCodesCheckBoxEditor").Click();
                MySession.FindElementByAccessibilityId("nextButton").Click();

                MySession.SwitchTo().Window(MySession.WindowHandles[0]);
                MySession.FindElementByAccessibilityId("nextButton").Click();

                MySession.SwitchTo().Window(MySession.WindowHandles[0]);
                MySession.FindElementByAccessibilityId("finishButton").Click();

                Thread.Sleep(40000);
                Thread.Sleep(10000);
                //Thread.Sleep(10000);

                MySession.SwitchTo().Window(MySession.WindowHandles[0]);
                MySession.FindElementByName("&Fermer").Click();
                


        }

        [TestMethod]
        public void Test_Demo_File()
        {


            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("OpenDemoFolderLabel").Click();
            


        }

        [TestMethod]
        public void Test_Open_Last_Folder()
        {
            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("OpenLastFolderLabel").Click();
            
        }


        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
