﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Fiche_Article : GesComPME
    {
        private const string AlwaysStartWith = "Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]";
        private const string TheFirstWindowOfMeca = "Window[@AutomationId=\"HostForm\"]";
        private const string TheImpoMenuOfAnyWindow = "Group[@Name =\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        private const string xpath_parametre = "Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        int i;

        [TestMethod]
        [TestCategory("N40")]
        public void Creation_Facture()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();


            // double cliquer sur ajouter
            action.DoubleClick(MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"1 personnalisé\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ajouter\"]")).Perform();

            // la fiche facture
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            //saisir le code tiers
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"thirdIdStringLookupEditor\"]/Edit[starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("FR00004");

            // ajouter des articles

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Tree[@AutomationId=\"linesTreeList\"]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud0\"]/DataItem[@Name=\"Code article row 0\"]").Click();

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Tree[@AutomationId=\"linesTreeList\"]/Edit[@Name=\"Commande d&apos;édition\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ouvrir\"]").Click();

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"list\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 6\"]/DataItem[@Name=\"Sélection row 5\"]").Click();


            //cliquer sur sélectionner
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"LookupBar\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Vues\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Sélectionner\"]").Click();

            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();


            MySession.FindElementByName("&Ok").Click();


            //  MySession.FindElementByXPath().Click();
            //  MySession.FindElementByXPath().SendKeys();

        }

        // *****************************************************************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Modification_Facture()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();
            // cliquer sur modifier
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"1 personnalisé\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Modifier\"]").Click();

            // modifier le fournisseur
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"thirdIdStringLookupEditor\"]/Edit[starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("CFR00003");
            // cliquer sur selectionner
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"LookupBar\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Vues\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Sélectionner\"]").Click();
            //   MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"list\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 3\"]/DataItem[@Name=\"Sélection row 2\"]").Click();
            //   MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Oui").Click();
            //  MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Oui").Click();
            //    MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Oui").Click();

            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();


        }


        //**************************************************************************************************************************

        [TestMethod]
        [TestCategory("N40")]
        public void Duplication_Facture()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);


            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();

            // cliquer droit 
            action.ContextClick().Perform();

            // cliquer sur dupliquer
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Dupliquer\"]").Click();

            MySession.FindElementByName("&Oui").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();

        }


        //**********************************************************************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Transfert_Facture()
        {
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);


            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();

            // double cliquer sur un bon de reception
            action.DoubleClick(MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Pane[@AutomationId=\"baseList\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Numéro du document row 0\"]")).Perform();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // cliquer sur transferer
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Barre d&apos;actions\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Transférer\"]").Click();


            Thread.Sleep(5000);
            // transfert en avoir
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // cliquer sur lancer
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[@AutomationId=\"DocumentTransferOperationForm\"][@Name=\"Transfert de document\"]/Pane[starts-with(@AutomationId,\"layoutManager\")][@Name=\"The XtraLayoutControl\"]/Button[@AutomationId=\"executeButton\"][@Name=\"Lancer\"]").Click();

            // cliquer sur fermer
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[@AutomationId=\"DocumentTransferOperationForm\"][@Name=\"Transfert de document\"]/Pane[starts-with(@AutomationId,\"layoutManager\")][@Name=\"The XtraLayoutControl\"]/Button[@AutomationId=\"closeButton\"][@Name=\"Fermer\"]").Click();


        }

        //*************************************************************************************************************************


        [TestMethod]
        [TestCategory("N40")]
        public void Regroupement_Facture()
        {
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();

            // cocher le deuxieme facture
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Pane[@AutomationId=\"baseList\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 2\"]/DataItem[@Name=\"Sélection row 1\"]").Click();

            // cliquer sur regrouper
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud3\"]/DataItem[@Name=\" row 3\"]").Click();

            // l'assistant
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // cliquer sur suivant
            //  MySession.FindElementByName("&Suivant >").Click();
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.FindElementByAccessibilityId("finishButton").Click();

            MySession.Keyboard.SendKeys(Keys.Alt + "f" + Keys.Alt);

            MySession.FindElementByName("&Non").Click();


        }
        //**********************************************************************************************************************

        [TestMethod]
        [TestCategory("N40")]
        public void MarquerCommeImprime_Facture()
        {
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();


            for (i = 0; i < 10; i++)
            {
                MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/ScrollBar[@Name=\"Barre de défilement\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ligne suivante\"]").Click();
            }

            // cliquer sur marquer comme imprimé
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud9\"]/DataItem[@Name=\" row 9\"]").Click();


            //  MySession.FindElementByXPath().Click();
            //  MySession.FindElementByXPath().SendKeys();

        }


        //**********************************************************************************************************************

        [TestMethod]
        [TestCategory("N40")]
        public void Validation_Facture()
        {
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();

            // cliquer sur valider
            for (i = 0; i < 10; i++)
            {
                MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/ScrollBar[@Name=\"Barre de défilement\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ligne suivante\"]").Click();
            }

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud11\"]/DataItem[@Name=\" row 11\"]").Click();



            MySession.FindElementByName("&Oui").Click();
        }


        //**********************************************************************************************************************

        [TestMethod]
        [TestCategory("N40")]
        public void Devalidation_Facture()
        {
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();

            // cliquer sur valider
            for (i = 0; i < 10; i++)
            {
                MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/ScrollBar[@Name=\"Barre de défilement\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ligne suivante\"]").Click();
            }

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud12\"]/DataItem[@Name=\" row 12\"]").Click();
            MySession.FindElementByName("&Oui").Click();
        }



        //**********************************************************************************************************************

        [TestMethod]
        [TestCategory("N40")]
        public void Ventilation_Facture()
        {
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();

            // cliquer sur valider
            for (i = 0; i < 10; i++)
            {
                MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/ScrollBar[@Name=\"Barre de défilement\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ligne suivante\"]").Click();
            }

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud15\"]/DataItem[@Name=\" row 15\"]").Click();

            // la fiche ventilation
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // cliquer sur appliquer le filtre
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ApproachChargeEntryForm\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Button[@AutomationId=\"applyFilterButton\"][@Name=\"Appliquer le filtre\"]").Click();

            // cocher quelques bons de réception et factures

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ApproachChargeEntryForm\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"documentsDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Sélection row 0\"]").Click();

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ApproachChargeEntryForm\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"documentsDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 4\"]/DataItem[@Name=\"Sélection row 3\"]").Click();

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ApproachChargeEntryForm\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"documentsDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 9\"]/DataItem[@Name=\"Sélection row 8\"]").Click();

            // cliquer sur calculer la répartition automatiquement
            MySession.FindElementByName("Calculer la répartition automatiquement").Click();

            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();





            //  MySession.FindElementByXPath().Click();
            //  MySession.FindElementByXPath().SendKeys();

        }


        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
