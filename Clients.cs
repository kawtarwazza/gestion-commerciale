﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Clients : GesComPME
    {
        private const string AlwaysStartWith = "Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]";
        private const string TheFirstWindowOfMeca = "Window[@AutomationId=\"HostForm\"]";
        private const string TheImpoMenuOfAnyWindow = "Group[@Name =\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        private const string xpath_parametre = "Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        int i;


        [TestMethod]
        [TestCategory("N40")]
        public void Creation_Client()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);
            // cliquer sur clients
            MySession.FindElementByAccessibilityId("customerNavigatorButton").Click();

            // double clique sur ajouter
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();

            // la fiche client
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // saisir nom 
            MySession.FindElementByAccessibilityId("nametextEditor").SendKeys("manal");

            // saisir adresse de facturation
            MySession.FindElementByAccessibilityId("invoicingAddressZipCodeStringLookupEditor").SendKeys("10000");

            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();






        }


        //******************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Modification_Client()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);
            // cliquer sur clients
            MySession.FindElementByAccessibilityId("customerNavigatorButton").Click();

            //cliquer sur modifier
            MySession.FindElementByName("Modifier").Click();

            // la fiche client 
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // cliquer sur l'onglet adresse
            MySession.FindElementByName("Adresses").Click();

            // ajouter une adresse de livraison
            //MySession.FindElementByName().Click();

            MySession.Keyboard.SendKeys(Keys.Control + "j" + Keys.Control);

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ThirdEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"addressesDataGrid\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Détail\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ajouter\"]").Click();

            // saisir l'adresse de lirvaison
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ThirdEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"addressesDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 2\"]/DataItem[@Name=\"Type livraison row 1\"]").Click();

            MySession.FindElementByName("Principale de livraison").Click();

            MySession.FindElementByName("&Oui").Click();

            MySession.FindElementByName("Code postal row 1").Click();
            MySession.FindElementByName("Code postal row 1").SendKeys("57001");


            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();


        }
        //********************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Affichage_Client()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur clients
            MySession.FindElementByAccessibilityId("customerNavigatorButton").Click();

            // double cliquer sur le dernier client
            action.DoubleClick(MySession.FindElementByName("Code (tiers) row 0")).Perform();

            // cliquer sur fermer
            // MySession.FindElementByName("Fermer").Click();


            //  MySession.FindElementByXPath().Click();
            //  MySession.FindElementByXPath().SendKeys();

            //  MySession.FindElementByAccessibilityId().Click();
            // MySession.FindElementByName().Click();



        }
        //***************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Creation_Famille_Client()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur famille client
            for (i = 0; i < 8; i++)
            {
                MySession.FindElementByName("Ligne suivante").Click();

            }

            MySession.FindElementByName(" row 20").Click();

            // cliquer sur ajouter
            MySession.FindElementByName("Ajouter").Click();

            // la fiche famille client
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // saisir le code famille
            MySession.FindElementByAccessibilityId("IdTextEditor").SendKeys("8");
            MySession.FindElementByAccessibilityId("CaptionTextEditor").SendKeys("Fam1");

            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();


        }

        //***************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Creation_sous_Famille_Client()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur sous famille client
            for (i = 0; i < 8; i++)
            {
                MySession.FindElementByName("Ligne suivante").Click();

            }

            MySession.FindElementByName(" row 21").Click();

            // cliquer sur ajouter
            MySession.FindElementByName("Ajouter").Click();

            // la fiche famille client
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("thirdSubFamilyCaptionTextEditor").SendKeys("sousfam12");
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"CustomerSubFamilyEntryForm\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"thirdFamilyIdStringLookupEditor\"]/Button[@Name=\"Ouvrir\"]").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"CustomerSubFamilyEntryForm\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"LookupBar\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Vues\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Sélectionner\"]").Click();

            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();

        }



        //***************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Creation_Client_avec_Famille()
        {
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur clients
            MySession.FindElementByAccessibilityId("customerNavigatorButton").Click();

            // double clique sur ajouter
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();

            // la fiche client
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // saisir nom 
            MySession.FindElementByAccessibilityId("nametextEditor").SendKeys("driss");

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ThirdEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"thirdFamilyStringLookupEditor\"]/Button[@Name=\"Ouvrir\"]").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ThirdEntryFormBase\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"LookupBar\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Vues\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Sélectionner\"]").Click();
            MySession.FindElementByName("&Oui").Click();


            // saisir adresse de facturation
            MySession.FindElementByAccessibilityId("invoicingAddressZipCodeStringLookupEditor").SendKeys("81000");

            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();
        }



        //***************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Statistiques()
        {
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur clients
            MySession.FindElementByAccessibilityId("customerNavigatorButton").Click();

            // selectionner tous les clients
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Pane[@AutomationId=\"baseList\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau d&apos;entête\"]/Header[@Name=\"Sélection\"]").Click();

            MySession.FindElementByName("Nœud5").Click();

            // la fiche statistiques
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // cliquer sur graphique
            MySession.FindElementByName("Graphique").Click();

            // recliquer sur sélection
            MySession.FindElementByName("Sélection").Click();

            // cliquer sur importer
            MySession.FindElementByName("Importer").Click();
            // cliquer sur valider
            MySession.FindElementByName("Valider").Click();
            // puis sur fermer
            MySession.FindElementByName("Fermer").Click();

        }

        //***************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Création_Facture_Periodique()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);


            // cliquer sur facture periodique
            for (i = 0; i < 10; i++)
            {

                MySession.FindElementByName("Ligne suivante").Click();

            }
            MySession.FindElementByName(" row 24").Click();

            // cliquer sur ajouter
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();

            // la fiche facture periodique
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // saisir le libellé
            MySession.FindElementByAccessibilityId("captionTextEditor").SendKeys("facper888");

            // choisir le document 
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"PeriodicInvoicingEntryForm\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"referenceDocumentGuidLookupEditor\"]/Button[@Name=\"Ouvrir\"]").Click();
            MySession.FindElementByName("Sélectionner").Click();
            MySession.FindElementByName("&Oui").Click();

            // cocher option général de la pièce
            MySession.FindElementByAccessibilityId("UpdatePricesCheckBoxEditor").Click();
            MySession.FindElementByAccessibilityId("keepUnitPriceProgramActiveCheckBoxEditor").Click();

            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();


        }

        //***************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Ajout_Affaires()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur affaires
            for (i = 0; i < 10; i++)
            {

                MySession.FindElementByName("Ligne suivante").Click();

            }
            MySession.FindElementByName(" row 25").Click();

            // Cliquer sur ajouter
            MySession.FindElementByName("Ajouter").Click();

            // la fiche affaires
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // saisir le libellé
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"DealEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"labelTextEditor\"]/Edit[starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("aff1");

            //ajouter un client
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"DealEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"customersDataGrid\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Détail\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ajouter\"]").Click();

            // selectionner un client
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"DealEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"customersDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Code client row 0\"]").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"DealEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"customersDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Code client row 0\"]").SendKeys("CCL00015");

            // Cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();
        }

        //***************************************************************************
        [TestMethod]
        [TestCategory("N40")]
        public void Ajout_Projets()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur affaires
            for (i = 0; i < 10; i++)
            {

                MySession.FindElementByName("Ligne suivante").Click();

            }
            MySession.FindElementByName(" row 26").Click();

            // Cliquer sur ajouter
            MySession.FindElementByName("Ajouter").Click();

            // la fiche projet
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // saisir le libellé
            MySession.FindElementByAccessibilityId("constructionSiteCaptionTextEditor").SendKeys("projet1");

            // choisir le client 
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"DealEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"customerIdStringLookupEditor\"]/Edit[starts-with(@ClassName,\"WindowsForms10\")]").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"DealEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"customerIdStringLookupEditor\"]/Edit[starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("CCL00015");

            // depot
            MySession.FindElementByAccessibilityId("storehouseIdGuidLookupEditor").SendKeys("principal");

            // Cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();


            //  MySession.FindElementByXPath().Click();
            //  MySession.FindElementByXPath().SendKeys();

            //  MySession.FindElementByAccessibilityId().Click();
            // MySession.FindElementByName().Click();

        }



        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
