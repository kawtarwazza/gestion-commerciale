﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class fiche_achat : GesComPME
    {

        private const string AlwaysStartWith = "Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]";
        private const string TheFirstWindowOfMeca = "Window[@AutomationId=\"HostForm\"]";
        private const string TheImpoMenuOfAnyWindow = "Group[@Name =\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        private const string xpath_parametre = "Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        int i;
        [TestMethod]
        [TestCategory("N40")]
        public void Creation_Demande_De_Prix()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Actions action = new Actions(MySession);

            // cliquer sur Achat
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Achats\"]").Click();

            // cliquer sur demande de prix
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"PurchasesWorkflowPage\"]/Pane[@AutomationId=\"MainNavigatorPanel\"][@Name=\"MainNavigatorPanel\"]/Pane[@AutomationId=\"QuoteNavigatorButton\"]").Click();

            // double cliquer sur ajouter
            action.DoubleClick(MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"1 personnalisé\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ajouter\"]")).Perform();

            // la fiche demande de prix
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            //saisir le code tiers
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"thirdIdStringLookupEditor\"]/Edit[starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("FR00004");

            // ajouter des articles

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Tree[@AutomationId=\"linesTreeList\"]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud0\"]/DataItem[@Name=\"Code article row 0\"]").Click();

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Tree[@AutomationId=\"linesTreeList\"]/Edit[@Name=\"Commande d&apos;édition\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ouvrir\"]").Click();

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"list\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 6\"]/DataItem[@Name=\"Sélection row 5\"]").Click();

            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"list\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Sélection row 0\"]").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Tree[@AutomationId=\"linesTreeList\"]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud0\"]/DataItem[@Name=\"Quantité row 0\"]").SendKeys("5");
            //cliquer sur sélectionner
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"LookupBar\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Vues\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Sélectionner\"]").Click();

            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();
            //  MySession.FindElementByXPath().Click();
            //  MySession.FindElementByXPath().SendKeys();

        }



        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
