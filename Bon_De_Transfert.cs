﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Bon_De_Transfert : GesComPME
    {

        [TestMethod]
        public void Test_Create_Bon_Entree()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);

            //Cliquer sur Stock / Articles
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            //MySession.FindElementByName("Stocks / Articles").Click();
            //Cliquer sur documents de stock
            action.DoubleClick(MySession.FindElementByName("Documents de stock")).Perform();


            //Cliquer sur Bon d'entrée
            MySession.FindElementByName("Bons d'entrée").Click();

            //Cliquer sur Ajouter
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("documentSerialStringLookupEditor").SendKeys(" ");
            MySession.FindElementByName("Code série row 0").Click();


            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Insérer").Click();
            action.DoubleClick(MySession.FindElementByName("Ligne Bien")).Perform();

            MySession.FindElementByName("Code article row 0").SendKeys(" ");
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Libellé row 0").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Quantité row 0").Click();
            MySession.FindElementByName("Quantité row 0").SendKeys("100");
            MySession.FindElementByName("Enregistrer et Fermer").Click();

        }
        [TestMethod]
       public void Test_Creation_Bon_Transfert()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);

            //Cliquer sur Stock / Articles
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            //MySession.FindElementByName("Stocks / Articles").Click();
            //Cliquer sur documents de stock
            action.DoubleClick(MySession.FindElementByName("Documents de stock")).Perform();
            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            action.DoubleClick(MySession.FindElementByAccessibilityId("TitleBar")).Perform();
            MySession.FindElementByName("Maximize").Click();

            Thread.Sleep(1000);
            //Cliquer sur Bon d'entrée
            MySession.FindElementByName("Bons de transfert").Click();

            //Cliquer sur Ajouter
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("documentSerialStringLookupEditor").SendKeys(" ");
            MySession.FindElementByName("Code série row 0").Click();

            
            //Depot destination
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("targetStorehouseGuidLookupEditor").Click();
            MySession.FindElementByAccessibilityId("targetStorehouseGuidLookupEditor").SendKeys(" ");
            action.DoubleClick(MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"StockDocumentEntryForm\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"LookupBar\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Vues\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ajouter\"]")).Perform();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("storehouseNameTextEditor").Click();
            MySession.FindElementByAccessibilityId("storehouseNameTextEditor").SendKeys("Depot de destination");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("address1TextEditor").Click();
            MySession.FindElementByAccessibilityId("address1TextEditor").SendKeys("Rue accasia");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("zipcodeStringLookupEditor").Click();
            MySession.FindElementByAccessibilityId("zipcodeStringLookupEditor").SendKeys("Barenton cel");
            MySession.FindElementByName("Ville row 0").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Insérer").Click();
            action.DoubleClick(MySession.FindElementByName("Ligne Bien")).Perform();
            MySession.FindElementByName("Code article row 0").SendKeys(" ");
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Libellé row 0").Click();



            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();
        }


        [TestMethod]
        public void Test_Modif_Bon_Transfert()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            //MySession.FindElementByName("Stocks / Articles").Click();
            //Cliquer sur documents de stock
            action.DoubleClick(MySession.FindElementByName("Documents de stock")).Perform();
            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            action.DoubleClick(MySession.FindElementByAccessibilityId("TitleBar")).Perform();
            MySession.FindElementByName("Maximize").Click();


            //Cliquer sur Bon d'entrée
            MySession.FindElementByName("Bons de transfert").Click();

            //Rechercher notre Bon d'entrée 
            MySession.FindElementByName("<Rechercher>").Click();
            MySession.FindElementByName("<Rechercher>").SendKeys("AB");
            MySession.FindElementByName("Numéro du document row 0").Click();
            // action.DoubleClick(MySession.FindElementByName("Numéro du document row 0")).Perform();
            MySession.FindElementByName("Modifier").Click();

            //On va ajouter depot de transit
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("transitStorehouseGuidLookupEditor").SendKeys(" ");
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"StockDocumentEntryForm\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"LookupBar\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Vues\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ajouter\"]").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("storehouseNameTextEditor").Click();
            MySession.FindElementByAccessibilityId("storehouseNameTextEditor").SendKeys("Depot transit 1");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("address1TextEditor").Click();
            MySession.FindElementByAccessibilityId("address1TextEditor").SendKeys("Rue accasia");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("zipcodeStringLookupEditor").Click();
            MySession.FindElementByAccessibilityId("zipcodeStringLookupEditor").SendKeys("Barenton cel");
            MySession.FindElementByName("Ville row 0").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();


            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            action.DoubleClick(MySession.FindElementByAccessibilityId("transitEndDateDateTimeEditor")).Perform();
            MySession.FindElementByAccessibilityId("transitEndDateDateTimeEditor").SendKeys("  ");
            MySession.FindElementByAccessibilityId("transitEndDateDateTimeEditor").SendKeys("20/07/2019");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

           
            //Vérification du changement
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Numéro du document row 0").Click();
            //action.DoubleClick(MySession.FindElementByName("Numéro du document row 0")).Perform();
            MySession.FindElementByName("Modifier").Click();
            String value = MySession.FindElementByName("transitDurationNumberEditor").GetAttribute("transitDurationNumberEditor");
            if (value.Equals("120"))
            {
                MySession.FindElementByName("Fermer").Click();

            }

        }
        [TestMethod]
        public void Test_Supp_Bon_Transfert()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            //MySession.FindElementByName("Stocks / Articles").Click();
            //Cliquer sur documents de stock
            action.DoubleClick(MySession.FindElementByName("Documents de stock")).Perform();
            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            action.DoubleClick(MySession.FindElementByAccessibilityId("TitleBar")).Perform();
            MySession.FindElementByName("Maximize").Click();


            //Cliquer sur Bon d'entrée
            MySession.FindElementByName("Bons de transfert").Click();

            //Rechercher notre Bon d'entrée 
            MySession.FindElementByName("<Rechercher>").Click();
            MySession.FindElementByName("<Rechercher>").SendKeys("AB");
            MySession.FindElementByName("Numéro du document row 0").Click();
            MySession.FindElementByName("Supprimer").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Oui").Click();
        }

        [TestMethod]
        public void Test_Supp_Bon_Entree()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);

            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            //MySession.FindElementByName("Stocks / Articles").Click();
            //Cliquer sur documents de stock
            action.DoubleClick(MySession.FindElementByName("Documents de stock")).Perform();


            //Cliquer sur Bon d'entrée
            MySession.FindElementByName("Bons d'entrée").Click();

            //Rechercher notre Bon d'entrée 
            MySession.FindElementByName("<Rechercher>").Click();
            MySession.FindElementByName("<Rechercher>").SendKeys("AB");
            MySession.FindElementByName("Numéro du document row 0").Click();
            MySession.FindElementByName("Supprimer").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Oui").Click();

        }

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
