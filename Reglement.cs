﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Reglement : GesComPME
    {
        private const string AlwaysStartWith = "Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]";
        private const string TheFirstWindowOfMeca = "Window[@AutomationId=\"HostForm\"]";
        private const string TheImpoMenuOfAnyWindow = "Group[@Name =\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        private const string xpath_parametre = "Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        int i;

        [TestMethod]
        [TestCategory("N40")]
        public void Creation_Reglement_Depuis_Facture()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            Actions action = new Actions(MySession);


            // cliquer sur achats
            MySession.FindElementByXPath("/" + TheFirstWindowOfMeca + "/" + TheImpoMenuOfAnyWindow + "/MenuItem[@Name=\"Achats\"]").Click();

            //Cliquer sur l'élément "Documents d'achat"
            WindowsElement DocumentAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Documents d&apos;achat\"]");
            MySession.Mouse.Click(DocumentAchat.Coordinates);

            // cliquer sur facture
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Factures\"]").Click();

            // cliquer sur reglement
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Tree[starts-with(@ClassName,\"WindowsForms10\")]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud6\"]/DataItem[@Name=\" row 6\"]").Click();

            // la fiche reglement 
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // cliquer sur enregistrer et fermer
            MySession.FindElementByName("Enregistrer et Fermer").Click();

            //  MySession.FindElementByXPath().Click();
            //  MySession.FindElementByXPath().SendKeys();

            //  MySession.FindElementByAccessibilityId().Click();
            // MySession.FindElementByName().Click();

        }

        //********************************************************************************************

        [TestMethod]
        [TestCategory("N40")]
        public void Reglement_Facture_avec_Avoir()
        {

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            Actions action = new Actions(MySession);

            //  MySession.FindElementByXPath().Click();
            //  MySession.FindElementByXPath().SendKeys();

            //  MySession.FindElementByAccessibilityId().Click();
            // MySession.FindElementByName().Click();

        }


        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
