﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EBP_GestionCom
{
    public class GesComPME
    {
        public const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";
        public const string PME_ID = "C:\\Program Files\\EBP\\Invoicing11.0FRFR40\\EBP.Invoicing.Application.exe" ;
        protected static WindowsDriver<WindowsElement> MySession;
        protected static RemoteTouchScreen touchScreen;






        public static void Setup(TestContext context)
        {
            //Il faut tuer le process de l'application en cours avant d’en relancer une nouvelle
            if (MySession == null)
            {
                string processName = "EBP.Invoicing.Application";
                //Il faut tuer le process avant d’en relancer une nouvelle
                foreach (Process process in Process.GetProcessesByName(processName))
                {
                    process.Kill();
                }
                Thread.Sleep(2000);
                LaunchApp();
            }
            else
            {
                LaunchApp();
            }
        }

        public static void LaunchApp()
        {
            //TearDown();
            // Creer une nouvelle session
            DesiredCapabilities appCapabilities = new DesiredCapabilities();
            appCapabilities.SetCapability("app", PME_ID);
            appCapabilities.SetCapability("deviceName", "WindowsPC");
            MySession = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appCapabilities);
            touchScreen = new RemoteTouchScreen(MySession);

            //Thread.Sleep(13000);
            MySession.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1.5));
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            Thread.Sleep(20000);
          //   MySession.SwitchTo().Window(MySession.WindowHandles[0]);
           //  MySession.FindElementByAccessibilityId("OpenLastFolderLabel").Click();
        }

        public static void TearDown()
        {

            //Fermer l'application et supprimer la session
            if (MySession != null)
            {
                MySession.FindElementByName("Close").Click();
                MySession.Quit();
                MySession = null;
            }
        }

       /* public virtual void PageAcceuil()
        {
            Thread.Sleep(5000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            //Maximiser la page d'acceuil
            MaximiserFenetres();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            //Cliquer sur le Menu Affichage
            string Affichage = "/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Affichage\"]";
            var winElemAffichage = MySession.FindElementByXPath(Affichage);
            winElemAffichage.Click();

            //CLiquer sur l'element "Écran de démarrage"
            WindowsElement accueil = MySession.FindElementByName("Écran de démarrage");
            MySession.Mouse.Click(accueil.Coordinates);

        }*/

        public static void MaximiserFenetres()
        {
            //Maximiser la page ouvrante
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);
        }








    }
}
