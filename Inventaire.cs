﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Inventaire : GesComPME
    {
        [TestMethod]
        public void Test_Creation_Inventaire()
        {
            //MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            //Cliquer sur Stock / Articles

            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            //MySession.FindElementByName("Stocks / Articles").Click();
            //Cliquer sur documents de stock
            action.DoubleClick(MySession.FindElementByName("Documents de stock")).Perform();


            //Cliquer sur Inventaires
            MySession.FindElementByName("Inventaires").Click();

            //Cliquer sur Ajouter
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            //Selectionner "Doit être transféré en compta"
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("mustBeTransferedToAccountingCheckBoxEditor").Click();
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("finishButton").Click();

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Fermer").Click();

            //Enregistrer et fermer l'inventaire
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();
        }

        [TestMethod]
        public void Test_Creation_Inventaire_Bis()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            //Cliquer sur Stock / Articles

            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            //MySession.FindElementByName("Stocks / Articles").Click();
            //Cliquer sur documents de stock
            action.DoubleClick(MySession.FindElementByName("Documents de stock")).Perform();


            //Cliquer sur Inventaires
            MySession.FindElementByName("Inventaires").Click();

            //Cliquer sur Ajouter
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            //Sur l'onglet Ordonnez vos données 
            //Au niveau de la partie "Regroupement", sélectionner autre chose que "Aucun"

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("groupingImageComboEditor").SendKeys("Code fournisseur");
           // MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Window[@AutomationId=\"InventoryWizardForm\"][@Name=\"Inventaire de stock\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/List[starts-with(@ClassName,\"WindowsForms10\")]/ListItem[@Name=\"Code fournisseur\"]").Click();
            MySession.FindElementByAccessibilityId("nextButton").Click();


            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("finishButton").Click();

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Fermer").Click();

            //Enregistrer et fermer l'inventaire
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();


        }




        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
