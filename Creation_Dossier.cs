﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Creation_Dossier : GesComPME
    {
        private const string AlwaysStartWith = "Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]";
        private const string TheFirstWindowOfMeca = "Window[@AutomationId=\"HostForm\"]";
        private const string TheImpoMenuOfAnyWindow = "Group[@Name =\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        private const string xpath_parametre = "Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        int i;


        [TestMethod]
        [TestCategory("N40")]
        public void CréerDossier()
        {
            Thread.Sleep(6000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            //Maximiser la page ouvrante
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            // Cliquer sur "Affichage"
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Affichage\"]").Click();

            // Cliquer sur le bouton écran d'accueil
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Écran d&apos;accueil\"]").Click();

            // Créer un nouveau dossier
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Window[@AutomationId=\"WelcomeDialog\"][@Name=\"Bienvenue\"]/Pane[starts-with(@AutomationId,\"layoutManager\")][@Name=\"The XtraLayoutControl\"]/Table[@Name=\"Root\"]/DataItem[@Name=\"Ouvrir le dossier de démonstration\"]").Click();

        }

        [TestMethod]
        [TestCategory("N40")]
        public void OuvrirDossierDeDémonstration()
        {
            Thread.Sleep(6000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            //Maximiser la page ouvrante
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            // Cliquer sur "Affichage"
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Affichage\"]").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Écran d&apos;accueil\"]").Click();

            // Ouvrir un dossier de démonstration
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Window[@AutomationId=\"WelcomeDialog\"][@Name=\"Bienvenue\"]/Pane[starts-with(@AutomationId,\"layoutManager\")][@Name=\"The XtraLayoutControl\"]/Table[@Name=\"Root\"]/DataItem[@Name=\"Ouvrir le dossier de démonstration\"]").Click();

            // le dossier démo est ouvert


        }

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
