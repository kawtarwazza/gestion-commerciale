﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Creation_Donnees_Bis : GesComPME
    {
        private const string AlwaysStartWith = "Pane[@Name=\"Bureau 1\"][@ClassName=\"#32769\"]";
        private const string TheFirstWindowOfMeca = "Window[@AutomationId=\"HostForm\"]";
        private const string TheImpoMenuOfAnyWindow = "Group[@Name =\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        private const string xpath_parametre = "Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]";
        int i;


        [TestMethod]
        [TestCategory("N40")]
        public void creer_donnees()
        {
            Thread.Sleep(6000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);


            Actions action = new Actions(MySession);
            // création d'un client

            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@AutomationId,\"MainWorkflowPage\")]/Pane[@AutomationId=\"MainNavigatorPanel\"][@Name=\"MainNavigatorPanel\"]/Pane[@AutomationId=\"customerNavigatorButton\"]").Click();

            // cliquer sur ajouter
            action.DoubleClick(MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"1 personnalisé\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ajouter\"]")).Perform();

            // fiche client
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ThirdEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"nametextEditor\"]").SendKeys("driss");

            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ThirdEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"invoicingAddressZipCodeStringLookupEditor\"]/Edit[starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("10000");

            MySession.Keyboard.SendKeys(Keys.Control + "e" + Keys.Control);

            // création d'un article
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Stocks / Articles\"]").Click();

            // cliquer sur l'icone article
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@AutomationId,\"StockWorkflowPage\")]/Pane[@AutomationId=\"MainNavigatorPanel\"][@Name=\"MainNavigatorPanel\"]/Pane[@AutomationId=\"ItemNavigatorButton\"]").Click();

            // action.DoubleClick(MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"1 personnalisé\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ajouter\"]")).Perform();
            // clique droit 
            action.ContextClick().Perform();
            // cliquer sur ajouter
            action.DoubleClick(MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ajouter\"]")).Perform();

            // MySession.FindElementByXPath().Click();
            // la fiche article
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // saisir le code
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"idTextEditor\"]/Edit[starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("1558");
            // saisir le libellé
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"captionTextEditor\"]/Edit[starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("article1");
            // saisir le prix d'achat
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"purchasePriceNumberEditor\"]/Edit[@Name=\"0,00\"][starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("10");

            // cliquer sur l'onglet stock
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@Name=\"Root\"]/Tab[@Name=\"Article\"]/TabItem[@Name=\"Stock\"]").Click();

            // quantite min 
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"stockItemDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Quantité minimum en stock row 0\"]").SendKeys("1");

            // quantite max
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"stockItemDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Quantité maximum en stock row 0\"]").SendKeys("1000");


            // cliquer sur l'onglet fournisseur
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@Name=\"Root\"]/Tab[@Name=\"Article\"]/TabItem[@Name=\"Fournisseurs\"]").Click();

            // ajouter un fournisseur
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"suppliersDataGrid\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Détail\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ajouter\"]").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"suppliersDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Code fournisseur row 0\"]").Click();

            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@AutomationId=\"suppliersDataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Code fournisseur row 0\"]").SendKeys("CFR00003");



            MySession.Keyboard.SendKeys(Keys.Control + "e" + Keys.Control);

        }
        [TestMethod]
        [TestCategory("N40")]
        public void depot_stock()
        {
            Thread.Sleep(6000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);


            Actions action = new Actions(MySession);

            // cliquer sur parametres
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Paramètres\"]").Click();

            // cliquer sur stock
            WindowsElement boutonAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stock\"]");
            MySession.Mouse.Click(boutonAchat.Coordinates);


            // cliquer sur depot
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Dépôts\"]").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            // création d'un depot
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"1 personnalisé\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ajouter\"]").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // saisir le libellé
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"StorehouseEntryForm\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Edit[@AutomationId=\"storehouseNameTextEditor\"]").SendKeys("ST1");

            // cliquer sur enregistrer et fermer
            MySession.Keyboard.SendKeys(Keys.Control + "e" + Keys.Control);



        }
        [TestMethod]
        [TestCategory("N40")]
        public void suppression_donnees()
        {
            // suppression du client
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);


            Actions action = new Actions(MySession);
            // suppression du client

            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@AutomationId,\"MainWorkflowPage\")]/Pane[@AutomationId=\"MainNavigatorPanel\"][@Name=\"MainNavigatorPanel\"]/Pane[@AutomationId=\"customerNavigatorButton\"]").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"1 personnalisé\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"&lt;Rechercher&gt;\"]").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"1 personnalisé\"][starts-with(@ClassName,\"WindowsForms10\")]/Edit[@Name=\"Commande d&apos;édition\"][starts-with(@ClassName,\"WindowsForms10\")]/Edit[@Name=\"Commande d&apos;édition\"][starts-with(@ClassName,\"WindowsForms10\")]").SendKeys("driss");
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"1 personnalisé\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Supprimer\"]").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Window[@AutomationId=\"FrmTaskDialog\"]/Pane[@AutomationId=\"pnlButtons\"][@Name=\"pnlButtons\"]/Button[starts-with(@AutomationId,\"bt\")][@Name=\"&amp;Oui\"]").Click();

            // suppression de l'article
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Stocks / Articles\"]").Click();

            // cliquer sur l'icone article
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@AutomationId,\"StockWorkflowPage\")]/Pane[@AutomationId=\"MainNavigatorPanel\"][@Name=\"MainNavigatorPanel\"]/Pane[@AutomationId=\"ItemNavigatorButton\"]").Click();


            // supprimer le premier article 
            action.DoubleClick(MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"][@Name=\"EBP Gestion Commerciale Ligne PME Open Line\"]/Pane[@AutomationId=\"ListPage\"]/Pane[@AutomationId=\"baseList\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Code article row 0\"]")).Perform();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Barre d&apos;outils\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Supprimer\"]").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"ItemEntryFormBase\"][@Name=\"Article [1558] Article1\"]/Window[@AutomationId=\"FrmTaskDialog\"]/Pane[@AutomationId=\"pnlButtons\"][@Name=\"pnlButtons\"]/Button[starts-with(@AutomationId,\"bt\")][@Name=\"&amp;Oui\"]").Click();

            // supprimer le depot de stock

            // cliquer sur parametres
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Paramètres\"]").Click();

            // cliquer sur stock
            WindowsElement boutonAchat = MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stock\"]");
            MySession.Mouse.Click(boutonAchat.Coordinates);


            // cliquer sur depot
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Dépôts\"]").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // decocher le premier depot
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Pane[@AutomationId=\"baseList\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 1\"]/DataItem[@Name=\"Sélection row 0\"]").Click();
            // cocher le depot ST1
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[@AutomationId=\"ListPage\"]/Pane[@AutomationId=\"baseList\"]/Table[@AutomationId=\"dataGrid\"]/Custom[@Name=\"Panneau de données\"]/Custom[@Name=\"Ligne 3\"]/DataItem[@Name=\"Sélection row 2\"]").Click();
            action.ContextClick().Perform();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Supprimer\"]").Click();

            //  MySession.FindElementByXPath().Click();
            //  MySession.FindElementByXPath().SendKeys();

        }




        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
