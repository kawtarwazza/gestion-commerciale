﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Creation_Donnees : GesComPME
    {
        [TestMethod]
        public void Test_Creation_Bien()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Articles")).Perform();

            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Biens").Click();

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("captionTextEditor").SendKeys("Bien 1");
            MySession.FindElementByAccessibilityId("purchasePriceNumberEditor").SendKeys("100");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();



        }

        [TestMethod]
        public void Test_Creation_Service()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Articles")).Perform();

            MySession.FindElementByName("Services").Click();
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();
            

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("captionTextEditor").SendKeys("Service 1");
            MySession.FindElementByAccessibilityId("purchasePriceNumberEditor").SendKeys("1500");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

        }

        

        [TestMethod]
        public void Test_Creation_Client()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ventes\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Clients")).Perform();
            Thread.Sleep(10000);
            MySession.FindElementByName("Clients").Click();
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nametextEditor").SendKeys("Client 1");
            MySession.FindElementByAccessibilityId("invoicingAddressAddress1TextEditor").SendKeys("Rue majorelle");
            MySession.FindElementByAccessibilityId("invoicingAddressZipCodeStringLookupEditor").SendKeys("pargny filain");
            MySession.FindElementByName("Ville row 0").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();


        }

        [TestMethod]
        public void Test_Creation_Prospect()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ventes\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Clients")).Perform();
            Thread.Sleep(10000);
            MySession.FindElementByName("Prospects").Click();
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nametextEditor").SendKeys("Prospect 1");
            MySession.FindElementByAccessibilityId("invoicingAddressAddress1TextEditor").SendKeys("Rue la paix");
            MySession.FindElementByAccessibilityId("invoicingAddressZipCodeStringLookupEditor").SendKeys("Braye en laonnois");
            MySession.FindElementByName("Ville row 0").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

        }

        [TestMethod]
        public void Test_Creation_Nomenclature_Fabrication()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Articles")).Perform();

            MySession.FindElementByName("Nomenclatures de fabrication").Click();
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();


            Thread.Sleep(1000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("captionTextEditor").SendKeys("NMF 1");
            MySession.FindElementByAccessibilityId("purchasePriceNumberEditor").SendKeys("2000");

            MySession.FindElementByName("Nomenclature").Click();
            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Code article row 0").Click();
            MySession.FindElementByName("Code article row 0").SendKeys("Bien 1");
            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Code article row 1").Click();
            MySession.FindElementByName("Code article row 0").SendKeys("Service 1");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

        }

        [TestMethod]
        public void Test_Creation_Nomenclature_Commerciale()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Articles")).Perform();

            MySession.FindElementByName("Nomenclatures commerciales").Click();
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();


            Thread.Sleep(1000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("captionTextEditor").SendKeys("NMC 1");
            MySession.FindElementByAccessibilityId("purchasePriceNumberEditor").SendKeys("2000");

            MySession.FindElementByName("Nomenclature").Click();
            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Code article row 0").Click();
            MySession.FindElementByName("Code article row 0").SendKeys("Bien 1");
            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Code article row 1").Click();
            MySession.FindElementByName("Code article row 0").SendKeys("Service 1");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

        }
        [TestMethod]
        public void Test_Creation_Fournisseur()
        {

        }

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
