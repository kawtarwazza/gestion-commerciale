﻿using System;
using System.Data.SqlClient;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Scenario_Test : GesComPME
    {
       [TestMethod]
       public void Test_Scenario()
        {
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            Actions action = new Actions(MySession);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Créer un nouveau dossier").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("databaseNameEditor").SendKeys("ScenarioT1");
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nameTextEditor").SendKeys("EBPMA");

            MySession.FindElementByAccessibilityId("address1TextEditor").SendKeys("Rue de la tour");
            MySession.FindElementByAccessibilityId("zipCodeTextEditor").SendKeys("78120");
            MySession.FindElementByAccessibilityId("cityTextEditor").SendKeys("Rambouillet");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("siretTextEditor").SendKeys("73282932000074");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("importZipCodesCheckBoxEditor").Click();
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("finishButton").Click();

            Thread.Sleep(40000);
            Thread.Sleep(10000);
            //Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Fermer").Click();
            Thread.Sleep(10000);
            //Maximiser la page ouvrante
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ventes\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Clients")).Perform();
            Thread.Sleep(10000);
            MySession.FindElementByName("Clients").Click();
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nametextEditor").SendKeys("Essylor");
            MySession.FindElementByAccessibilityId("invoicingAddressAddress1TextEditor").SendKeys("Rue majorelle");
            MySession.FindElementByAccessibilityId("invoicingAddressZipCodeStringLookupEditor").SendKeys("pargny filain");
            MySession.FindElementByName("Ville row 0").Click();

            MySession.FindElementByAccessibilityId("civilityStringLookupEditor").SendKeys("Monsieur");
            MySession.FindElementByName("Civilité row 0").Click();

            MySession.FindElementByAccessibilityId("invoicingContactCivilityStringLookupEditor").SendKeys("Monsieur");
            MySession.FindElementByName("Civilité row 0").Click();

            MySession.FindElementByAccessibilityId("invoicingContactFirstNameTextEditor").SendKeys("Karim");
            MySession.FindElementByAccessibilityId("invoicingContactLastNameTextEditor").SendKeys("Wazzani");

            MySession.FindElementByAccessibilityId("invoicingContactFunctionTextEditor").SendKeys("Directeur IT");
            MySession.FindElementByAccessibilityId("invoicingContactPhoneTextEditor").SendKeys("0766457896");

            MySession.FindElementByName("Livraison").Click();
            MySession.FindElementByAccessibilityId("useSameAddressCheckBox").Click();

            Thread.Sleep(1000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Oui").Click();

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("deliveryAddressCivilityStringLookupEditor").SendKeys("Monsieur");
            MySession.FindElementByName("Civilité row 0").Click();

            MySession.FindElementByAccessibilityId("deliveryAddressNameTextEditor").SendKeys("Othmane");
            MySession.FindElementByAccessibilityId("deliveryAddressAddress1TextEditor").SendKeys("Rue la paix");

            MySession.FindElementByAccessibilityId("deliveryAddressZipCodeStringLookupEditor").SendKeys("Dijon");
            MySession.FindElementByName("Ville row 0").Click();

            MySession.FindElementByName("Gestion").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("discountRateNumberEditor").Click();
            MySession.FindElementByAccessibilityId("discountRateNumberEditor").SendKeys("5");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Divers").Click();

            MySession.FindElementByAccessibilityId("settlementModeStringLookupEditor").Click();
            MySession.FindElementByAccessibilityId("settlementModeStringLookupEditor").SendKeys("Chèque en 2 fois");
            MySession.FindElementByName("Sélectionner").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

            //MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            //MySession.FindElementByName("&Ok").Click();
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            //  MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);
           
            //MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            //MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Stocks / Articles\"]").Click();
             MySession.FindElementByName("Stocks / Articles").Click();
           // MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Stocks / Articles\"]").Click();
            action.DoubleClick(MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@AutomationId,\"StockWorkflowPage\")]/Pane[@AutomationId=\"MainNavigatorPanel\"][@Name=\"MainNavigatorPanel\"]/Pane[@AutomationId=\"ItemNavigatorButton\"]")).Perform();

            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Biens").Click();

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("captionTextEditor").SendKeys("Verre de lunettes");
            MySession.FindElementByAccessibilityId("purchasePriceNumberEditor").SendKeys("10 ");
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
           // MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Services").Click();
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();


            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("captionTextEditor").SendKeys("Montage");
            MySession.FindElementByAccessibilityId("purchasePriceNumberEditor").SendKeys("13");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();


            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            //MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Affichage\"]").Click();
            MySession.FindElementByName("Affichage").Click();
            //MySession.FindElementByName("Écran de démarrage").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Écran de démarrage\"]").Click();
            Thread.Sleep(1000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Pane[starts-with(@AutomationId,\"MainWorkflowPage\")]/Pane[@AutomationId=\"MainNavigatorPanel\"][@Name=\"MainNavigatorPanel\"]/Pane[@AutomationId=\"CivilityNavigatorButton\"]").Click();

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Devis").Click();
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("thirdNameStringLookupEditor").SendKeys("Essylor");
            MySession.FindElementByName("Code (tiers) row 0").Click();

            //MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            //MySession.FindElementByName("&Oui").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Ligne Bien").Click();
            MySession.FindElementByName("Code article row 0").Click();
           // MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Tree[@AutomationId=\"linesTreeList\"]/Group[@Name=\"Panneau de données\"]/TreeItem[@Name=\"Nœud0\"]/DataItem[@Name=\"Code article row 0\"]").Click();
            MySession.FindElementByName("Code article row 0").SendKeys(" ");
            //MySession.FindElementByName("Biens").Click();
            MySession.FindElementByName("Libellé row 0").Click();
            MySession.FindElementByName("Quantité row 0").Click();
            MySession.FindElementByName("Quantité row 0").SendKeys("15");

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);


            MySession.FindElementByName("Récapitulatif").Click();
            MySession.FindElementByName("Récapitulatif").Click();
            //MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"][@Name=\"Devis du 26/07/2019 (Nouveau)\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@Name=\"Root\"]/Tab[@Name=\"Detail\"]/TabItem[@Name=\"Récapitulatif\"]").Click();
            MySession.FindElementByAccessibilityId("shippingAmountVatExcludedNumberEditor").Click();
            MySession.FindElementByAccessibilityId("shippingAmountVatExcludedNumberEditor").SendKeys("10");
            MySession.FindElementByAccessibilityId("depositAmountNumberEditor").SendKeys("17");

            MySession.FindElementByName("Transférer").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Ok").Click();

            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("documentTypeImageComboEditor").SendKeys("Facture");
            //MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            Thread.Sleep(10000);
            MySession.FindElementByAccessibilityId("openCreatedDocumentsCheckBoxEditor").Click();

            MySession.FindElementByAccessibilityId("executeButton").Click();

            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("OK").Click();

            Thread.Sleep(8000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("closeSimpleButton").Click();


            Thread.Sleep(8000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("openCreatedDocumentsCheckBoxEditor").Click();
            MySession.FindElementByAccessibilityId("closeButton").Click();

            Thread.Sleep(10000);
            Thread.Sleep(10000);
            /* MySession.SwitchTo().Window(MySession.WindowHandles[0]);
             MySession.FindElementByAccessibilityId("openCreatedDocumentsCheckBoxEditor").Click();

             MySession.FindElementByAccessibilityId("closeSimpleButton").Click();*/


            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            /* MySession.FindElementByName("Code article row 2").Click();
             MySession.FindElementByName("Code article row 2").SendKeys(" ");
             MySession.FindElementByName("Services").Click();
             MySession.FindElementByName("Libellé row 0").Click();
             */
            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Ligne Service").Click();
            MySession.FindElementByName("Code article row 1").Click();
            MySession.FindElementByName("Code article row 1").SendKeys(" ");
            MySession.FindElementByName("Libellé row 0").Click();
            Thread.Sleep(8000);
            MySession.FindElementByName("Quantité row 1").Click();
            MySession.FindElementByName("Quantité row 1").SendKeys("2");

            /* MySession.SwitchTo().Window(MySession.WindowHandles[0]);
             MySession.FindElementByAccessibilityId("closeSimpleButton").Click();
             Thread.Sleep(8000);*/
            //MySession.FindElementByName("Echéances/Déductions").Click();
          /*  MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Pane[@AutomationId=\"layoutManager\"][@Name=\"The XtraLayoutControl\"]/Table[@Name=\"Root\"]/Tab[@Name=\"Detail\"]/TabItem[@Name=\"Echéances/Déductions\"]").Click();
            MySession.FindElementByName("Ajouter").Click();
            //action.DoubleClick(MySession.FindElementByName("Date échéance row 0")).Perform();
            MySession.FindElementByName("Date échéance row 0").Click();
            MySession.FindElementByName("Date échéance row 0").SendKeys("31/08/2019");
            //action.DoubleClick(MySession.FindElementByName("Montant row 0")).Perform();
            MySession.FindElementByName("Montant row 0").Click();
            MySession.FindElementByName("Montant row 0").SendKeys("12");
            MySession.FindElementByName("Répartir").Click();
            */
           // MySession.FindElementByName("Actions").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu Principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Actions\"]").Click();
            //MySession.FindElementByName("Régler la facture").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Régler la facture\"]").Click();
            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Ok").Click();
            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("referenceTextEditor").SendKeys("AB");
            MySession.FindElementByAccessibilityId("accountingTransferModeImageComboEditor").SendKeys("En tant que règlement");

            MySession.FindElementByAccessibilityId("BankStringLookupEditor").SendKeys(" ");
            
            MySession.FindElementByName("Ajouter").Click();
            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("IdTextEditor").SendKeys("123");
            MySession.FindElementByAccessibilityId("CaptionTextEditor").SendKeys("CIH");
            MySession.FindElementByAccessibilityId("bankNameTextEditor").SendKeys("CIH Banque");
            
            MySession.FindElementByName("Comptabilité").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("BankBookTextEditor").SendKeys("123");

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Ok").Click();

            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Fermer").Click();

            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Fermer").Click();



        }



        public bool Verification_Client()
        {
            string connString = "Server=LAPTOP-KFRNKTOT/EBP;uid=LAPTOP-KFRNKTOT/kawtar;Password=;Database=ScenarioT1_0895452f-b7c1-4c00-a316-c6a6d0ea4bf4";
            string query = "select * from Customer where Name = 'Essylor'" ;
            bool existe = false;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            existe = true;
                            break;
                        }
                    }
                }
            }
            return existe;
        }


        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
