﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace EBP_GestionCom
{
    [TestClass]
    public class Devis_Vente : GesComPME
    {
      
        [TestMethod]
        public void Test_Creation_Devis()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
           // MySession.FindElementByName("Ventes").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ventes\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Documents de vente")).Perform();
            
            MySession.FindElementByName("Devis").Click();
            action.DoubleClick(MySession.FindElementByName("Ajouter")).Perform();
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("thirdIdStringLookupEditor").Click();
            MySession.FindElementByAccessibilityId("thirdIdStringLookupEditor").SendKeys("Client 1");
            MySession.FindElementByName("Type de tiers row 0").Click();
            MySession.FindElementByAccessibilityId("civilityStringLookupEditor").SendKeys("Monsieur");
            MySession.FindElementByName("Civilité row 0").Click();
            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Ligne Bien").Click();
            MySession.FindElementByName("Code article row 0").Click();
            MySession.FindElementByName("Code article row 0").SendKeys(" ");
            MySession.FindElementByName("Code barre row 0").Click();
            
            Thread.Sleep(10000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();


        }

        [TestMethod]
        public void Test_Modif_Devis()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // MySession.FindElementByName("Ventes").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ventes\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Documents de vente")).Perform();

            MySession.FindElementByName("Devis").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("<Rechercher>").Click();
            MySession.FindElementByName("Numéro du document row 0").Click();
            MySession.FindElementByName("Modifier").Click();

            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            //On ajoute le service au Devis
            MySession.FindElementByName("Ajouter").Click();
            MySession.FindElementByName("Ligne Service").Click();
            MySession.FindElementByName("Code article row 1").Click();
            MySession.FindElementByName("Code article row 1").SendKeys(" ");
            MySession.FindElementByName("Code barre row 0").Click();
            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();

        }

        [TestMethod]
        public void Test_Supp_Devis()
        {


            Actions action = new Actions(MySession);
            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // MySession.FindElementByName("Ventes").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ventes\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Documents de vente")).Perform();

            MySession.FindElementByName("Devis").Click();


            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Numéro du document row 0").Click();
            MySession.FindElementByName("Supprimer").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Oui").Click();

        }

        [TestMethod]
        public void Test_Dupliquer_Devis()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // MySession.FindElementByName("Ventes").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ventes\"]").Click();
            action.DoubleClick(MySession.FindElementByName("Documents de vente")).Perform();

            MySession.FindElementByName("Devis").Click();
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Numéro du document row 0").Click();
            MySession.FindElementByName("Dupliquer").Click();


            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("&Oui").Click();

            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("documentSerialStringLookupEditor").SendKeys(" ");
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"TradeDocumentEntryFormBase\"]/Window[starts-with(@ClassName,\"WindowsForms10\")]/Pane[@AutomationId=\"LookupBar\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/ToolBar[@Name=\"Vues\"][starts-with(@ClassName,\"WindowsForms10\")]/Button[@Name=\"Ajouter\"]").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("serialNumberTextEditor").SendKeys("AV");
            MySession.FindElementByAccessibilityId("serialCaptionTextEditor").SendKeys("Serie 1");
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();


            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByName("Enregistrer et Fermer").Click();


        }

        [TestMethod]
        public void Test_Regrouper_Devis()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);


            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // MySession.FindElementByName("Ventes").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ventes\"]").Click();
            Thread.Sleep(1000);

            action.DoubleClick(MySession.FindElementByName("Documents de vente")).Perform();

            MySession.FindElementByName("Devis").Click();
            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
           // MySession.FindElementByName("Sélection row 0").Click();
            MySession.FindElementByName("Sélection row 1").Click();
            action.ContextClick(MySession.FindElementByName("Panneau de données")).Perform();
            //MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Regrouper\"]").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("nextButton").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("finishButton").Click();


            Thread.Sleep(10000);
            Thread.Sleep(10000);

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);

            MySession.FindElementByAccessibilityId("closeSimpleButton").Click();
        }

        [TestMethod]
        public void Test_Devis_En_Facture()
        {
            Actions action = new Actions(MySession);
            Thread.Sleep(10000);
            MySession.Keyboard.SendKeys(Keys.Command + Keys.ArrowUp + Keys.Command);


            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // MySession.FindElementByName("Ventes").Click();
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Window[@AutomationId=\"HostForm\"]/Group[@Name=\"Arrimer en haut\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuBar[@Name=\"Menu principal\"][starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Ventes\"]").Click();
            Thread.Sleep(1000);

            action.DoubleClick(MySession.FindElementByName("Documents de vente")).Perform();

            MySession.FindElementByName("Devis").Click();
            Thread.Sleep(1000);
            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            // MySession.FindElementByName("Sélection row 0").Click();
            MySession.FindElementByName("Sélection row 1").Click();
            action.ContextClick(MySession.FindElementByName("Panneau de données")).Perform();
            //MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByXPath("/Pane[@Name=\"Desktop 1\"][@ClassName=\"#32769\"]/Pane[starts-with(@ClassName,\"WindowsForms10\")]/Menu[starts-with(@ClassName,\"WindowsForms10\")]/MenuItem[@Name=\"Transférer\"]").Click();

            MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("documentTypeImageComboEditor").SendKeys("Facture");

            //MySession.SwitchTo().Window(MySession.WindowHandles[0]);
            MySession.FindElementByAccessibilityId("executeButton").Click();

            Thread.Sleep(10000);
            Thread.Sleep(10000);
        }
        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Setup(context);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TearDown();
        }
       /* [TestInitialize]
        public override void PageAcceuil()
        {
            // revenir à la page d'acceuil
            base.PageAcceuil();
        }*/
    }
}
